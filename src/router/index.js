import { createRouter, createWebHistory } from "vue-router";

// Páginas
import Dashboard from "@/views/dashboard/Dashboard.vue";

// Autenticación
import Login from "@/views/Auth/Login.vue";
import Logout from "@/views/Auth/Logout.vue";

// Listas
import Granjas from "@/views/Listas/Granjas.vue";
import Canales from "@/views/Listas/Canales.vue";



// Consultas
import ConsultarGranja from "@/views/Consultar/ConsultarGranja.vue";
import ConsultarCanal from "@/views/Consultar/ConsultarCanal.vue";




// Layouts
import AuthLayout from "@/layouts/AuthLayout.vue";
import MainLayout from "@/layouts/MainLayout.vue";


//Errores
import NotFound from "@/views/Errores/NotFound.vue";
import NotAuthorized from "@/views/Errores/NotAuthorized.vue";
import ErrorServidor from "@/views/Errores/ErrorServidor.vue";
import store from "../store";



const routes = [
  // #region Autenticación
  {
    path: "/",
    name: "Login",
    component: Login,
    meta: { layout: AuthLayout },
  },
  {
    path: "/logout",
    name: "Logout",
    component: Logout,
  },
  // #endregion

  // #region Paginas

  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
    meta: { layout: MainLayout },
  },
 
  // #endregion

  // #region Listas

  {
    path: "/granja",
    name: "Granjas",
    component: Granjas,
    meta: { layout: MainLayout },
  },
  {
    path: "/canal",
    name: "Canales",
    component: Canales,
    meta: { layout: MainLayout },
  },
  // #endregion

  // #region Consultas
  
  {
    path: "/granja/:id",
    name: "Granja",
    component: ConsultarGranja,
    meta: { layout: MainLayout},
  },
  {
    path: "/canal/:id",
    name: "Canal",
    component: ConsultarCanal,
    meta: { layout: MainLayout},
  },
  

  // #endregion

 
  // #region Errores
  // 404
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: NotFound,
  },
  //403 No autorizado
  {
    path: "/403",
    name: "NotAuthorized",
    component: NotAuthorized,
  },
  //500 Error de servidor
  {
    path: "/500",
    name: "ErrorServidor",
    component: ErrorServidor,
  },
  // #endregion
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

// Verifica si el usuario está autenticado
router.beforeEach((to, _, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // El usuario no está autenticado
    if (localStorage.getItem("token") === null) {
            next({
              path: "/",
              query: { redirect: to.fullPath },
            });
          } 
    
        
    
  }
  
  
  // Continúa con la navegación
  else next();
  
});

export default router;
