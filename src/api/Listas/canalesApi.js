import { ref } from "vue";
import axios from "axios";

const canalesApi = () => {
    const errorCanales = ref(null);
    const resultCanales = ref(null);
    const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      };
    const getCanales = async () => {
        try {
            // Post data to backend
            resultCanales.value = await axios.get("/canales/", config);
            // console.log(resultCanales.value)

            // Reset error Canales
            errorCanales.value = null;
        } catch (err) {
            errorCanales.value = err;
        }
    };

    return { getCanales, resultCanales, errorCanales };
};

export default canalesApi;
