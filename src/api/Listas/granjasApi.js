import { ref } from "vue";
import axios from "axios";

const granjasApi = () => {
    const errorGranjas = ref(null);
    const resultGranjas = ref(null);
    const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      };
    const getGranjas = async () => {
        try {
            // Post data to backend
            resultGranjas.value = await axios.get("/farm/", config);
            // console.log(resultGranjas.value)
            // Reset error Granjas
            errorGranjas.value = null;
        } catch (err) {
            errorGranjas.value = err;
        }
    };

    return { getGranjas, resultGranjas, errorGranjas };
};

export default granjasApi;
