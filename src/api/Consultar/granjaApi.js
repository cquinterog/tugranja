import { ref, onErrorCaptured } from "vue";
import axios from "axios";

const granjaApi = () => {
  const errorGranja = ref(null);
  const resultGranja = ref(null);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  const getGranja = async (id) => {
    try {
      // Obtiene los datos del backend
      const respuesta = await axios.get(`/farm/${id}`, config);
            

      // Almacena la respuesta
      resultGranja.value = respuesta.data;
      // console.log(resultGranja.value)
      

      // Reset errorGranja
      errorGranja.value = null;
    } catch (err) {

      errorGranja.value = err.response;
      onErrorCaptured.value = err.response.data.message;
      
    }
  };

  return { getGranja, resultGranja, errorGranja };
};

export default granjaApi;
