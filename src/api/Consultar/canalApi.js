import { ref, onErrorCaptured } from "vue";
import axios from "axios";

const canalApi = () => {
  const errorCanal = ref(null);
  const resultCanal = ref(null);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  const getCanal = async (id) => {
    try {
      // Obtiene los datos del backend
      const respuesta = await axios.get(`/canales/${id}`, config);
// console.log(respuesta)
      // Almacena la respuesta
      resultCanal.value = respuesta.data;
      
      

      // Reset errorCanal
      errorCanal.value = null;
    } catch (err) {

      errorCanal.value = err.response;
      onErrorCaptured.value = err.response.data.message;
    }
  };

  return { getCanal, resultCanal, errorCanal };
};

export default canalApi;
