import { ref } from "vue";
import axios from "axios";

const crearCanalApi = () => {
  const errorCrearCanal = ref(null);
  const resultCrearCanal = ref(null);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  const crearCanal = async (name, type, crops) => {
    try {
      // Post data to backend
      const params = new URLSearchParams();
      params.append("name", name);
      params.append("type", type);
      params.append("crops", crops);
      resultCrearCanal.value = await axios.post("canales/", params, config);
      // Reset error
      errorCrearCanal.value = null;
    } catch (err) {
      errorCrearCanal.value = err.response;
    }
  };

  return { crearCanal, resultCrearCanal, errorCrearCanal };
};

export default crearCanalApi;
