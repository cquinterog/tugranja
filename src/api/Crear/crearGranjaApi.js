import { ref } from "vue";
import axios from "axios";

const crearGranjaApi = () => {
  const errorCrearGranja = ref(null);
  const resultCrearGranja = ref(null);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  const crearGranja = async (name, owner, crops) => {
    
    try {
      // Post data to backend
      const params = new URLSearchParams();
      params.append("name", name);
      params.append("owner", owner);
      params.append("crops", crops);
      resultCrearGranja.value = await axios.post("farm/", params, config);
      // Reset error
      errorCrearGranja.value = null;
    } catch (err) {
      errorCrearGranja.value = err.response;
      console.log(errorCrearGranja.value)
    }

  };

  return { crearGranja, resultCrearGranja, errorCrearGranja };
};

export default crearGranjaApi;
