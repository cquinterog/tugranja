import { ref } from "vue";
import axios from "axios";

const loginApi = () => {
  const error = ref(null);
  const result = ref(null);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  const login = async (username, password) => {
    // console.log(username), console.log(password);
    try {
      // Post data to backend
      const params = new URLSearchParams();
      params.append("username", username);
      params.append("password", password);
      result.value = await axios.post("auth/",params, config);
      // console.log(result.value)
      // Reset error
      error.value = null;
    } catch (err) {
      error.value = err.response.data.mensaje;
    }
  };

  return { login, result, error };
};

export default loginApi;
