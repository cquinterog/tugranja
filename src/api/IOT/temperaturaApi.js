import { ref, onErrorCaptured } from "vue";
import axios from "axios";

const temperaturaApi = () => {
  const errorTemperatura = ref(null);
  const resultTemperatura = ref(null);
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  const getTemperatura = async (id,timeAgo) => {
    try {
      // Obtiene los datos del backend
      const respuesta = await axios.get(`/iot/${id}/${timeAgo}`, config);

      // Almacena la respuesta
      resultTemperatura.value = respuesta.data;
      
      

      // Reset errorTemperatura
      errorTemperatura.value = null;
    } catch (err) {

      errorTemperatura.value = err.response;
      onErrorCaptured.value = err.response.data.message;
    }
  };

  return { getTemperatura, resultTemperatura, errorTemperatura };
};

export default temperaturaApi;
