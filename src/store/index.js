import { createStore } from "vuex";
import { decodificarToken } from "../utils/token";

export default createStore({
  state: {
    token: null,
    titulo: "",
    paginaActiva: "",
  },
  mutations: {
    setTitulo(state, titulo) {
      state.titulo = titulo;
    },
    setPaginaActiva(state, paginaActiva) {
      state.paginaActiva = paginaActiva;
    },
    setUser(state, token) {
      // Guarda los datos en localStorage
      localStorage.setItem("token", token);

      // Decodifica la información del usuario
      const usuario = decodificarToken(token);

      

      // Guarda los datos en el estado
      state.token = token;
    },
    resetUser(state) {
      // Elimina los datos del usuario
      state.token = null;
      // Elimina la información del usuario de localStorage
      localStorage.removeItem("token");
    },
    loadStateFromLocalStorage(state) {
      state.token = null;
      // Obtiene la información de localStorage
      
      if (localStorage.getItem("token")) {
        state.token = localStorage.getItem("token");
      }
    },
 
   
  },
  actions: {
    setUser(context, token) {
      context.commit("setUser", token);
    },
    resetUser(context) {
      context.commit("resetUser");
    },
    loadState(context) {
      context.commit("loadStateFromLocalStorage");
    },
    
  },
  getters: {
    getUser: (state) => () => {
      if (state.token === null) {
        return null;
      }

      return decodificarToken(state.token);
    },
   
  },
});
